import * as React from 'react';
import ModalWrapper from './ModalWrapper.jsx'
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";
import ModalBody from "./ModalBody.jsx";
import Button from "../Button.jsx";

import './Modal.scss'
import PropTypes from "prop-types";

const Modal = (props) => {
    const {
        open,
        onClose,
        header,
        modalBody,
        firstText,
        secondaryText,
        firstClick,
        secondaryClick} = props

    if (!open) return null
    return (
    <>
        <ModalWrapper>
            <div className="overlay_modal"></div>
        </ModalWrapper>

        <div className="modal">
            <ModalHeader secondaryText={secondaryText}>
                {header}
            </ModalHeader>
            <ModalBody>
                {modalBody}
            </ModalBody>
            <ModalFooter firstText={firstText} secondaryText={secondaryText} firstClick={firstClick} secondaryClick={secondaryClick} />
            <Button className="modal_close" onClick={onClose}>
                X
            </Button>
        </div>
    </>
    )
}
Modal.propTypes = {
    onClose: PropTypes.func,
    header: PropTypes.any,
    modalBody: PropTypes.any,
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
}
export default Modal