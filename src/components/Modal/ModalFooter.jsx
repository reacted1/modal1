import * as React from 'react';
import Button from '../Button.jsx'
import PropTypes from "prop-types";
const ModalFooter = (props) => {
    const {
        firstText,
        secondaryText,
        firstClick,
        secondaryClick
    } = props
    if (secondaryText && secondaryClick) {
        return (
            <>
                <footer className="button-list">
                    <Button className="btn btn-yes" onClick={firstClick}>
                        {firstText}
                    </Button>
                    <Button className="btn btn-no" onClick={secondaryClick}>
                        {secondaryText}
                    </Button>
                </footer>
            </>
        )
    } else {
        return (
            <>
                <footer>
                    <Button className="btn btn-no" onClick={firstClick}>
                        {firstText}
                    </Button>
                </footer>
            </>
        )
    }
}

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
}

export default ModalFooter