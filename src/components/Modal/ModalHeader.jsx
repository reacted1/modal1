import * as React from 'react';

const ModalHeader = ({children, secondaryText}) => {
    if (secondaryText) {
        return (
            <>
                <header >
                    <div className="img-container">
                    </div>
                    <p className="modal-content__title">{children}</p>
                </header>
            </>
        )
    }else {
        return (
            <>
                <header >
                    <p className="modal-content__title">{children}</p>
                </header>
            </>
        )
    }
}

export default ModalHeader