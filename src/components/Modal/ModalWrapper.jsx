import * as React from 'react';

const ModalWrapper = ({children}) => {
    if (children) {
        return (
            <>
                {children}
            </>
        )
    }
}

export default ModalWrapper