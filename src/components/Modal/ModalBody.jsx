import * as React from 'react';

const ModalBody = (props) => {
    const {
        className,
        children,
    } = props
    return (
        <>
            <div >
                <p className={className}>
                    {children}
                </p>
            </div>
        </>
    )
}

export default ModalBody