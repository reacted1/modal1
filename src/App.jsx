import React, { useState } from "react";
import Button from "./components/Button.jsx";
import Modal from './components/Modal/Modal.jsx';

import './App.scss'

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenSecond, setIsOpenSecond] = useState(false);
  return (
    <>
        <React.StrictMode>
            <h1>Choose an option</h1>
            <div className="btn-list">
                <Button className="_first" onClick={setIsOpen}>First Form</Button>
                <Button className="_second" onClick={setIsOpenSecond}>Second Form</Button>
            </div>
            <Modal open={isOpen} header="We got New" modalBody="Best price for best quality" firstText="Get Num" firstClick={() => {console.log(1); setIsOpen(false);}} secondaryText="Wow No" secondaryClick={()=> setIsOpen(false)} onClose={()=>setIsOpen(false)}>
            </Modal>
            <Modal open={isOpenSecond} header="Simple single String" modalBody="lorem..." firstText="Get Num" firstClick={() => {console.log(1); setIsOpenSecond(false);}} onClose={()=>setIsOpenSecond(false)}>
            </Modal>
        </React.StrictMode>
    </>
  )
}

export default App
